import 'dart:async';

class Login {
  int _id;
  String _access_token;
  int _expires_in;
  String _token_type;
  String _refresh_token;
  int initialTime;


  Login(this._id, this._access_token, this._expires_in, this._token_type,
      this._refresh_token);

}

class Contact {
  final String fullName;
  final String email;

  const Contact({this.fullName, this.email});
}

abstract class LoginRepository {
  Future<Login> doLogin(String username, String password);
}


class LoginException implements Exception {
  final _message;

  LoginException([this._message]);

  String toString() {
    if (_message == null) return "Exception";
    return "Exception: $_message";
  }
}