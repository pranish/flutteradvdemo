class ApiConstants {

  static final String RELEASE_UAT_AUTH_BASE_URL = "http://sipradiauth.ekbana.info/";
  static final String RELEASE_UAT_API_BASE_URL = "http://sipradiapi.ekbana.info/";

  static final String API_VERSION = "api/v1/";

  static final String LOGIN_URL = RELEASE_UAT_AUTH_BASE_URL + API_VERSION +
      "connect/token";

}