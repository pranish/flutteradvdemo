import 'package:flutter/material.dart';
import 'package:flutter_adv_demo/res/values/Strings.dart';
import 'package:flutter_adv_demo/view/HomePage.dart';
import 'LoginPage.dart';

void main() => runApp(new MyApp());


final routes = {
  '/login': (BuildContext context) => new LoginPage(),
  '/home': (BuildContext context) => new HomePage(),
  '/': (BuildContext context) => LoginPage()
};

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: Strings.titleLogin,
      theme: new ThemeData(primarySwatch: Colors.teal),
      routes: routes,
    );
  }

}