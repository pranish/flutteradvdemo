# Welcome
This repo is a demonstration on developing native android / ios apps using flutter. 

Go ahead and try:

```
$ git clone https://pranish@bitbucket.org/pranish/flutteradvdemo.git/wiki
```

# Branch Details
* ```dev``` - A merge branch for developers.

* ```001-MVP-SQFlite-Login-Setup``` - A basic MVP, sql database and login page setup (No api calls).

* ```002-BasicJsonParse``` - A demonstration on how to use dart ```http``` to parse a ```json``` ```GET``` response.

* ```003-Firebase-Auth``` - A demonstration on ```Firebase Authentication``` which includes ```Google Signin``` and * ```email/password``` signin.

# Libraries Used
* ```sqflite: 0.8.9``` for sqlite database
* ```path_provider: "^0.4.0"``` path provider for getting current database path of mobile device. Used to create new sqlite database
* ```firebase_auth: "^0.5.10"``` Firebase authentication
* ```google_sign_in: "^3.0.3+1"``` Google signin

Please report any issues or comment on the code if there is any errors or enhancements.

Have fun!


For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
